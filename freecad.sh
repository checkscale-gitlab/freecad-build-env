#!/bin/bash

fc_source={Source/Dir}
fc_build={Build/Dir}
other_files={otherfiles/dir}

#Launch the Docker image.

sudo podman run -it --rm \
-v $fc_source:/mnt/source \
-v $fc_build:/mnt/build \
-v $other_files:/mnt/files \
-e "DISPLAY" -e "QT_X11_NO_MITSHM=1" -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
docker.io/muxoid/freecad_dev:latest


